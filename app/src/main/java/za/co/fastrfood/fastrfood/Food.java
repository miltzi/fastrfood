package za.co.fastrfood.fastrfood;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Console;

/**
 * Created by Josh on 2015-04-19.
 */
public class Food extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_food_layout,container,false);
        String[] foods={"Big Mac#28.00","McChicken#21.50","McDouble#19.50","Chicken Nuggets 6pcs#18.00","Small Fries#12.00","Medium Fries#15.00","Large Fries#18.00"};

        ListView foodListView=(ListView) view.findViewById(android.R.id.list);

        ListAdapter foodAdapter=new CustomAdapter(getActivity(),foods);

        foodListView.setAdapter(foodAdapter);

        foodListView.setOnItemClickListener(
            new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                    String food=String.valueOf(parent.getItemAtPosition(position));
                    Order.AddItem(food,food.split("#")[1]);
                    Toast.makeText(getActivity(),"Added "+food.split("#")[0]+" to order.", Toast.LENGTH_SHORT).show();
                }
            }
        );
        return view;
    }



}
