package za.co.fastrfood.fastrfood;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by Josh on 2015-04-19.
 */
public class Meal extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_meal_layout,container,false);
        String[] meals={"Big Mac Medium Meal#43.00","Big Mac Large Meal#48.00","McRoyale Medium Meal#44.50","McRoyale Large Meal#49.50","McFeast Deluxe Medium Meal#55.00","McFeast Deluxe Large Meal#60.00"};

        ListView mealListView=(ListView) view.findViewById(android.R.id.list);

        ListAdapter mealAdapter=new CustomAdapter(getActivity(),meals);

        mealListView.setAdapter(mealAdapter);
        mealListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                        String meal=String.valueOf(parent.getItemAtPosition(position));
                        Order.AddItem(meal,meal.split("#")[1]);
                        Toast.makeText(getActivity(), "Added " + meal.split("#")[0] + " to order.", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        return view;
    }
}
