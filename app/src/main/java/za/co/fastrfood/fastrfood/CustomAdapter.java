package za.co.fastrfood.fastrfood;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Josh on 2015-04-19.
 */
public class CustomAdapter extends ArrayAdapter<String> {

    public CustomAdapter(Context context, String[] foods) {
        super(context,R.layout.custom_food_row ,foods);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater foodInflater=LayoutInflater.from(getContext());
        View customView=foodInflater.inflate(R.layout.custom_food_row, parent,false);
        String singleFoodItem=getItem(position);
        TextView foodText=(TextView)customView.findViewById(R.id.txtFoodName);
        TextView foodPrice=(TextView)customView.findViewById(R.id.txtFoodPrice);
        ImageView foodImage=(ImageView) customView.findViewById(R.id.imgFood);

        foodText.setText(singleFoodItem.split("#")[0]);
        foodPrice.setText("R"+Double.parseDouble(singleFoodItem.split("#")[1])+"0");
        Context context = foodImage.getContext();
        int id = context.getResources().getIdentifier(singleFoodItem.split("#")[0].toLowerCase().replace(" ",""), "drawable", context.getPackageName());
        foodImage.setImageResource(id);
        return customView;
    }
}
