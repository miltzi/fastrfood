package za.co.fastrfood.fastrfood;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Created by Josh on 2015-04-19.
 */
public class Drink extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_drink_layout,container,false);
        String[] drinks={"Small Coke#8.00","Medium Coke#11.00","Large Coke#14.00","Small Fanta#8.00","Medium Fanta#11.00","Large Fanta#14.00"};

        ListView drinkListView=(ListView) view.findViewById(android.R.id.list);

        ListAdapter drinkAdapter=new CustomAdapter(getActivity(),drinks);

        drinkListView.setAdapter(drinkAdapter);
        drinkListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent,View view,int position,long id){
                        String drink=String.valueOf(parent.getItemAtPosition(position));
                        Order.AddItem(drink,drink.split("#")[1]);
                        Toast.makeText(getActivity(), "Added " + drink.split("#")[0] + " to order.", Toast.LENGTH_SHORT).show();
                    }
                }
        );
        return view;
    }
}
