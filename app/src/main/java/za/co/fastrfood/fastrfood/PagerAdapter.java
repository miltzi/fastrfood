package za.co.fastrfood.fastrfood;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Josh on 2015-04-19.
 */
public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int arg0) {
        switch (arg0){
            case 0:
                return new Food();
            case 1:
                return new Meal();
            case 2:
                return new Drink();
        }
        return null;
    }

    @Override
    public int getCount(){
        return 3;
    }



}
