package za.co.fastrfood.fastrfood;

/**
 * Created by Josh on 2015-04-25.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Josh on 2015-04-19.
 */
public class CustomOrderAdapter extends ArrayAdapter<String> {

    public CustomOrderAdapter(Context context, String[] foods) {
        super(context,R.layout.custom_food_row ,foods);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater foodInflater=LayoutInflater.from(getContext());
        View customView=foodInflater.inflate(R.layout.custom_food_order_row, parent,false);
        final String singleFoodItem=getItem(position);
        TextView foodText=(TextView)customView.findViewById(R.id.txtFoodName);
        TextView foodPrice=(TextView)customView.findViewById(R.id.txtFoodPrice);
        ImageView foodImage=(ImageView) customView.findViewById(R.id.imgFood);

        ImageButton btnDelete=(ImageButton) customView.findViewById(R.id.btnDel);
        btnDelete.setTag(singleFoodItem.split("#")[0]);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Order.RemoveItem(singleFoodItem);

                Intent intent = new Intent(getContext(), MyOrder.class);
                getContext().startActivity(intent);
                ((Activity)getContext()).finish();
            }
        });

        foodText.setText(singleFoodItem.split("#")[0]);

        foodPrice.setText("R"+Double.parseDouble(singleFoodItem.split("#")[1])+"0");
        Log.v("help",foodPrice.getText().toString());
        Context context = foodImage.getContext();
        int id = context.getResources().getIdentifier(singleFoodItem.split("#")[0].toLowerCase().replace(" ",""), "drawable", context.getPackageName());
        foodImage.setImageResource(id);
        return customView;
    }


}
