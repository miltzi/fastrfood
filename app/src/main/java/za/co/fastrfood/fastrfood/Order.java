package za.co.fastrfood.fastrfood;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Josh on 2015-04-19.
 */
public class Order {
    public static List<String[]> order =new ArrayList<String[]>();

    public static void AddItem(String item,String price)
    {
        String[] entry=new String[2];
        entry[0]=item;
        entry[1]=price;
        order.add(entry);
        Log.v("Added to Order", item);
    }

    public static void RemoveItem(String item)
    {
        for (int i=0;i<order.size();i++)
        {
            if(order.get(i)[0].equalsIgnoreCase(item))
            {
                order.remove(i);
                break;
            }
        }
    }

    public static List<String[]> getOrder()
    {
        return order;
    }
    public static String displayOrder()
    {
        String items="Your Order: \n";
        for (int i=0;i<order.size();i++)
        {
            items+=order.get(i)[0]+"\n";

        }
        Log.v("fastfoodlog",items);
        return items;
    }

    public static String QROrder()
    {
        String data="";
        for (int i=0;i<order.size();i++)
        {
            data+=order.get(i)[0];
            if(i!=order.size()-1)
            {
                data+="|";
            }
        }
        Log.v("fastfoodlog",data);
        return data;
    }

    public static double getTotal()
    {
        List<String[]> order=getOrder();
        double total=0;
        for (int i=0;i<order.size();i++)
        {
            total+=Double.parseDouble(order.get(i)[1]);
            Log.v("fastfoodlog",""+Double.parseDouble(order.get(i)[1]));
        }
        return total;
    }
    public static void printOrderToLog()
    {
        Log.v("fastfoodlog","Current order: ");
        for (int i=0;i<order.size();i++)
        {
            Log.v("fastfoodlog","Item"+i+": "+order.get(i)[0]);
        }
    }
}
